FROM ubuntu:latest

RUN apt update
RUN apt install -y bind9  bind9-host dnsutils

COPY entrypoint.sh /sbin/entrypoint.sh
RUN chmod 755 /sbin/entrypoint.sh

#RUN mkdir /data

RUN rm -f /etc/bind/*

EXPOSE 53/udp 53/tcp
ENTRYPOINT ["/sbin/entrypoint.sh"]
#CMD ["/usr/sbin/named", "-u", "bind"]
